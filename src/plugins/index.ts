import { App } from "vue";
import { setupI18n } from "./i18n";
import { setupNaive } from "./naive-ui";
import { setupVxe } from "./vxe-table";
import echarts from "./echarts";
/*其它组件引用 */
import "@styles/reset.scss";
import "@styles/element.scss";
import "@assets/iconfont/iconfont.css";
import "@assets/css/style.css";
import "github-markdown-css/github-markdown.css";
import "@config/router.permission";
import "vite-plugin-svg-icons/register";
/**
 *
 * @param app
 */

export const  setupPlugins= (app:App)=>{
  setupNaive(app)
  setupVxe(app)
  setupI18n(app)
  app.use(echarts)
}
