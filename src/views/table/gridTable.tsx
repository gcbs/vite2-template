import { NButton } from "naive-ui";
import { defineComponent, provide, ref } from "vue";
import { VxeGridProps, Grid, VxeGridInstance, Button } from "vxe-table";
import tableModel from "@apis/table/table";
const gridTable = defineComponent({
  name: "gridTable",
  setup() {
    const xGrid = ref({} as VxeGridInstance);
    const searchSlot = row => {
      return [
        <div>
          <NButton class="mr-1">新增</NButton>
          <NButton class="mr-1">删除</NButton>
          <NButton class="mr-1">编辑</NButton>
        </div>,
      ];
    };
    const tableConfig: VxeGridProps = {
      border: "outer",
      data: [
        {
          name: "zs",
          nickname: "yyyyyyy",
          role: "admin",
          address: "圣地亚",
        },
      ],
      headerCellClassName: "table-header",
      loading: false,
      cellClassName: "table-cell",
      autoResize: true,
      resizable: true,
      toolbarConfig: {
        perfect: true,
        refresh: true,
        zoom: true,
        custom: true,
        className: "custom-toolbar",
        slots: {
          buttons: row => {
            return searchSlot(row);
          },
        },
      },
      pagerConfig: {
        pageSize: 10,
        layouts: [
          "PrevJump",
          "PrevPage",
          "JumpNumber",
          "NextPage",
          "NextJump",
          "Sizes",
          "Total",
        ],
      },
      columns: [
        { type: "checkbox", title: "#", width: 50 },
        { type: "seq", width: 60 },
        { field: "name", title: "Name" },
        { field: "nickname", title: "Nickname" },
        { field: "role", title: "Role" },
        { field: "address", title: "Address", showOverflow: true },
      ],
      checkboxConfig: {
        reserve: true,
      },
      proxyConfig: {
        seq: true, // 启用动态序号代理（分页之后索引自动计算为当前页的起始序号）
        props: {
          result: "list",
          total: "total",
        },
        ajax: {
          query: ({ page }) => {
            return tableModel.base({ ...page });
          },
        },
      },
    };
    return () => (
      <div>
        <Grid {...tableConfig}></Grid>
      </div>
    );
  },
});
export default gridTable;
