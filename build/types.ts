export interface Build {
  webTitle:string,
  needMock:boolean,
  injectCode?:string,
}
